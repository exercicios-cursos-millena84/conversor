#!/bin/bash
echo " "
echo " "
echo "==============================================================="
echo "===> CONVERSOR MONETARIO"
echo "="
echo "="

curl "https://api.exchangeratesapi.io/latest?base=$1&symbols=$2" > jsonConversao.json 2>/dev/null

echo "= VOCE PEDIU PARA CONVERTER: " `echo $3` " DINHEIROS, DE " `echo $1` " PARA " `echo $2`
echo "="

echo "= O VALOR DE 1 "`echo $1`" EM "`echo $2`" EH DE "`cat jsonConversao.json | jq .rates.$2`
echo "="

taxa=`cat jsonConversao.json | jq .rates.$2`
valor=$3

conversao=`python -c "print $valor*$taxa" ####`

echo "= E O VALOR FINAL CONVERTIDO EH: "`echo $conversao`
echo "==============================================================="

rm jsonConversao.json
